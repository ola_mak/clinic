package com.amak.clinic.service

import AppointmentIsTakenException
import PatientHasAppointmentException
import com.amak.clinic.converter.Converter
import com.amak.clinic.dto.VisitDTO
import com.amak.clinic.dto.VisitHourDTO
import com.amak.clinic.dto.VisitResponseDTO
import com.amak.clinic.repository.DoctorRepository
import com.amak.clinic.repository.PatientRepository
import com.amak.clinic.repository.Visit
import com.amak.clinic.repository.VisitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.EntityNotFoundException


@Service
class VisitServiceImpl : VisitService {

    private var visitRepository: VisitRepository
    private var patientRepository: PatientRepository
    private var doctorRepository: DoctorRepository
    private var converter: Converter

    @Autowired
    constructor(visitRepository: VisitRepository,
                patientRepository: PatientRepository,
                doctorRepository: DoctorRepository,
                converter: Converter) {
        this.visitRepository = visitRepository
        this.patientRepository = patientRepository
        this.doctorRepository = doctorRepository
        this.converter = converter
    }

    override fun getVisits(): List<VisitResponseDTO> {
        val visits = mutableListOf<VisitResponseDTO>()
        visitRepository.findAll().forEach { visit ->
            visits.add(converter.convertVisitEntityToResponseDTO(visit))
        }
        return visits;
    }

    override fun getVisitsByPatient(patientId: Long): List<VisitResponseDTO> {
        throwExceptionIfPatientIdIsIncorrect(patientId)
        throwExceptionIfThereIsNoVisitForPatient(patientId)
        return visitRepository.getVisitsByPatient(patientId)
                .map { patient -> converter.convertVisitEntityToResponseDTO(patient) }
    }

    private fun throwExceptionIfPatientIdIsIncorrect(id: Long) {
        if (patientRepository.findById(id).isEmpty) {
            throw EntityNotFoundException("There is no patient with given id")
        }
    }

    private fun throwExceptionIfThereIsNoVisitForPatient(id: Long) {
        if (visitRepository.getVisitsByPatient(id).isEmpty()) {
            throw EntityNotFoundException("There is no visits of this patient")
        }
    }

    override fun addVisit(patientId: Long, visitDTO: VisitDTO) {
        throwExceptionIfDoctorIdIsIncorrect(visitDTO.doctorId)

        var visit: Visit = converter.convertVisitDTOToEntity(visitDTO)

        patientRepository.findById(patientId)
                .map { patient -> visit.patient = patient }
                .orElseThrow { EntityNotFoundException("There is no patient with given id") }

        throwExceptionIfAppointmentIsTaken(visitDTO)
        throwExceptionIfPatientHasAppointmentWithinGivenPeriod(patientId, visitDTO)

        visitRepository.save(visit)
    }

    private fun throwExceptionIfDoctorIdIsIncorrect(id: Long) {
        if (doctorRepository.findById(id).isEmpty) {
            throw EntityNotFoundException("There is no doctor with given id")
        }
    }

    private fun throwExceptionIfAppointmentIsTaken(visitDTO: VisitDTO) {
        visitRepository.findByDoctorInGivenTime(visitDTO.doctorId, LocalDate.parse(visitDTO.date), LocalTime.parse(visitDTO.hour))
                ?.let { throw AppointmentIsTakenException() }
    }

    private fun throwExceptionIfPatientHasAppointmentWithinGivenPeriod(patientId: Long, visitDTO: VisitDTO) {
        visitRepository.findByPatientInGivenTime(patientId, LocalDate.parse(visitDTO.date), LocalTime.parse(visitDTO.hour))
                ?.let { throw PatientHasAppointmentException() }
    }

    override fun updateVisit(id: Long, visitHourDTO: VisitHourDTO) {
        visitRepository.findById(id)
                .ifPresentOrElse({ visit ->
                    run {
                        visit.hour = LocalTime.parse(visitHourDTO.hour)
                        visitRepository.save(visit)
                    }
                }, { throw EntityNotFoundException("There is no visit with given id") })
    }

    override fun deleteVisit(id: Long) {
        try {
            visitRepository.deleteById(id);
        } catch (e: EmptyResultDataAccessException) {
            throw EntityNotFoundException("There is no visit with given id")
        }
    }

}