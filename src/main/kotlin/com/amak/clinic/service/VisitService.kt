package com.amak.clinic.service

import com.amak.clinic.dto.VisitDTO
import com.amak.clinic.dto.VisitHourDTO
import com.amak.clinic.dto.VisitResponseDTO
import org.springframework.stereotype.Service

@Service
interface VisitService {
    fun getVisits(): List<VisitResponseDTO>
    fun getVisitsByPatient(patientId: Long): List<VisitResponseDTO>
    fun addVisit(patientId: Long, visitDTO: VisitDTO)
    fun updateVisit(id: Long, visitHourDTO: VisitHourDTO)
    fun deleteVisit(id: Long)
}