package com.amak.clinic.service

import com.amak.clinic.dto.PatientDTO
import com.amak.clinic.dto.PatientResponseDTO
import org.springframework.stereotype.Service

@Service
interface PatientService {
    fun getPatients(): List<PatientResponseDTO>
    fun getPatient(id: Long): PatientResponseDTO
    fun addPatient(patientDTO: PatientDTO)
    fun updatePatient(id: Long, patientDTO: PatientDTO)
    fun deletePatient(id: Long)
}