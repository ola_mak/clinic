package com.amak.clinic.service

import com.amak.clinic.dto.DoctorDTO
import com.amak.clinic.dto.DoctorResponseDTO
import org.springframework.stereotype.Service

@Service
interface DoctorService {
    fun getDoctors(): List<DoctorResponseDTO>
    fun getDoctor(id: Long): DoctorResponseDTO
    fun addDoctor(doctorDTO: DoctorDTO)
    fun updateDoctor(id: Long, doctorDTO: DoctorDTO)
    fun deleteDoctor(id: Long)
}