package com.amak.clinic.service

import com.amak.clinic.converter.Converter
import com.amak.clinic.dto.PatientDTO
import com.amak.clinic.dto.PatientResponseDTO
import com.amak.clinic.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class PatientServiceImpl : PatientService {

    private var patientRepository: PatientRepository
    private var converter: Converter

    @Autowired
    constructor(patientRepository: PatientRepository, converter: Converter) {
        this.patientRepository = patientRepository
        this.converter = converter
    }

    override fun getPatients(): List<PatientResponseDTO> {
        val patients = mutableListOf<PatientResponseDTO>()
        patientRepository.findAll().forEach { patient ->
            patients.add(converter.convertPatientEntityToResponseDTO(patient))
        }
        return patients;
    }

    override fun getPatient(id: Long): PatientResponseDTO {
        return patientRepository.findById(id)
                .map { patient -> converter.convertPatientEntityToResponseDTO(patient) }
                .orElseThrow { EntityNotFoundException("There is no patient with given id") }
    }

    override fun addPatient(patientDTO: PatientDTO) {
        patientRepository.save(converter.convertPatientDTOToEntity(patientDTO))
    }

    override fun updatePatient(id: Long, patientDTO: PatientDTO) {
        patientRepository.findById(id)
                .ifPresentOrElse({ patient ->
                    run {
                        patient.name = patientDTO.name
                        patient.surname = patientDTO.surname
                        patient.address = patientDTO.address
                        patientRepository.save(patient)
                    }
                }, { throw EntityNotFoundException("There is no patient with given id") })
    }

    override fun deletePatient(id: Long) {
        try {
            patientRepository.deleteById(id)
        } catch (e: EmptyResultDataAccessException) {
            throw EntityNotFoundException("There is no patient with given id")
        }
    }


}