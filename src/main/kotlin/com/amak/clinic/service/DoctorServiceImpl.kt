package com.amak.clinic.service

import com.amak.clinic.converter.Converter
import com.amak.clinic.dto.DoctorDTO
import com.amak.clinic.dto.DoctorResponseDTO
import com.amak.clinic.repository.DoctorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class DoctorServiceImpl : DoctorService {

    private var doctorRepository: DoctorRepository
    private var converter: Converter

    @Autowired
    constructor(doctorRepository: DoctorRepository, converter: Converter) {
        this.doctorRepository = doctorRepository
        this.converter = converter
    }

    override fun getDoctors(): List<DoctorResponseDTO> {
        val doctors = mutableListOf<DoctorResponseDTO>()
        doctorRepository.findAll().forEach { doctor ->
            doctors.add(converter.convertDoctorEntityToResponseDTO(doctor))
        }
        return doctors;
    }

    override fun getDoctor(id: Long): DoctorResponseDTO {
        return doctorRepository.findById(id)
                .map { doctor -> converter.convertDoctorEntityToResponseDTO(doctor) }
                .orElseThrow { EntityNotFoundException("There is no doctor with given id") }
    }

    override fun addDoctor(doctorDTO: DoctorDTO) {
        doctorRepository.save(converter.convertDoctorDTOToEntity(doctorDTO))
    }

    override fun updateDoctor(id: Long, doctorDTO: DoctorDTO) {
        doctorRepository.findById(id)
                .ifPresentOrElse({ doctor ->
                    run {
                        doctor.name = doctorDTO.name
                        doctor.surname = doctorDTO.surname
                        doctor.specialization = doctorDTO.specialization
                        doctorRepository.save(doctor)
                    }
                }, { throw EntityNotFoundException("There is no doctor with given id") })
    }

    override fun deleteDoctor(id: Long) {
        try {
            doctorRepository.deleteById(id);
        } catch (e: EmptyResultDataAccessException) {
            throw EntityNotFoundException("There is no doctor with given id")
        }
    }

}