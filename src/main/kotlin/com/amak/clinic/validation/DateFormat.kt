import java.lang.annotation.Documented
import javax.validation.Constraint
import kotlin.reflect.KClass


@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [DateFormatValidator::class])
annotation class DateFormat(
        val message: String = "Please use dd-MMM-yyyy format of date. The appointment can only be scheduled for the following days.",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Any>> = []
)