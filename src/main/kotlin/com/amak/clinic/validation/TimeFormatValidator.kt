import java.time.LocalTime
import java.time.format.DateTimeParseException
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class TimeFormatValidator : ConstraintValidator<TimeFormat?, String?> {
    override fun initialize(constraintAnnotation: TimeFormat?) {}
    override fun isValid(value: String?, constraintValidatorContext: ConstraintValidatorContext?): Boolean {
        try {
            val minutes = LocalTime.parse(value).minute
            return minutes == 0 || minutes == 30
        } catch (e: DateTimeParseException) {
            return false
        }
    }
}