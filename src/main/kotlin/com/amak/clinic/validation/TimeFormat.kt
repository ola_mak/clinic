import java.lang.annotation.Documented
import javax.validation.Constraint
import kotlin.reflect.KClass


@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [TimeFormatValidator::class])
annotation class TimeFormat(
        val message: String = "Please use hh:mm:ss format of time, appointment can be scheduled every half hour ",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Any>> = []
)