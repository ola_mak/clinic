import java.time.LocalDate
import java.time.format.DateTimeParseException
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext


class DateFormatValidator : ConstraintValidator<DateFormat?, String?> {
    override fun initialize(constraintAnnotation: DateFormat?) {}
    override fun isValid(value: String?, constraintValidatorContext: ConstraintValidatorContext?): Boolean {
        try {
            val date = LocalDate.parse(value)
            return date.isAfter(LocalDate.now())
        } catch (e: DateTimeParseException) {
            return false
        }
    }
}