package com.amak.clinic.dto

data class VisitResponseDTO(
        var id: Long = 0,
        var date: String = "",
        var hour: String = "",
        var place: String = "",
        var doctorId: Long = 0,
        var patientId: Long = 0
)