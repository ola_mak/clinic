package com.amak.clinic.dto

import TimeFormat

data class VisitHourDTO(
        @TimeFormat
        var hour: String = ""
)