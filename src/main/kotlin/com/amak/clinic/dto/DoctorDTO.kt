package com.amak.clinic.dto

import javax.validation.constraints.NotBlank

data class DoctorDTO(
        @field:NotBlank(message = "Name cannot be blank")
        var name: String = "",
        @field:NotBlank(message = "Surname cannot be blank")
        var surname: String = "",
        @field:NotBlank(message = "Specialization cannot be blank")
        var specialization: String = ""
)