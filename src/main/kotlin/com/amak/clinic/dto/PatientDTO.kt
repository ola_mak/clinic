package com.amak.clinic.dto

import javax.validation.constraints.NotBlank

data class PatientDTO(
        @field:NotBlank(message = "Name cannot be blank")
        var name: String = "",
        @field:NotBlank(message = "Surname cannot be blank")
        var surname: String = "",
        @field:NotBlank(message = "Address cannot be blank")
        var address: String = ""
)