package com.amak.clinic.dto

import DateFormat
import TimeFormat
import javax.validation.constraints.NotBlank

data class VisitDTO(
        @DateFormat
        var date: String = "",
        @TimeFormat
        var hour: String = "",
        @field:NotBlank(message = "Place cannot be blank")
        var place: String = "",
        var doctorId: Long = 0
)