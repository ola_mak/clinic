package com.amak.clinic.dto

data class DoctorResponseDTO(
        var id: Long = 0,
        var name: String = "",
        var surname: String = "",
        var specialization: String = ""
)