package com.amak.clinic.dto

data class PatientResponseDTO(
        var id: Long = 0,
        var name: String = "",
        var surname: String = "",
        var address: String = ""
)