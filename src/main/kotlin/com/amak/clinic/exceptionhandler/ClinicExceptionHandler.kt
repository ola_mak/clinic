package com.amak.clinic.exceptionhandler

import AppointmentIsTakenException
import PatientHasAppointmentException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.persistence.EntityNotFoundException

@ControllerAdvice
class ClinicExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler
    protected fun handleAppointmentIsTakenException(e: AppointmentIsTakenException): ResponseEntity<String?> {
        return ResponseEntity<String?>(e.message, HttpStatus.CONFLICT)
    }

    @ExceptionHandler
    protected fun handlePatientHasAppointmentException(e: PatientHasAppointmentException): ResponseEntity<String?> {
        return ResponseEntity<String?>(e.message, HttpStatus.CONFLICT)
    }

    @ExceptionHandler
    protected fun handleEntityNotFoundException(e: EntityNotFoundException): ResponseEntity<String?> {
        return ResponseEntity<String?>(e.message, HttpStatus.NOT_FOUND)
    }

    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException,
                                              headers: HttpHeaders, status: HttpStatus,
                                              request: WebRequest): ResponseEntity<Any> {
        val errorMessage = ex.bindingResult.fieldErrors.stream()
                .map { obj: FieldError -> obj.defaultMessage }
                .findFirst()
                .orElse(ex.message)
        return handleExceptionInternal(ex, errorMessage, headers, status, request)
    }
}