package com.amak.clinic

import com.amak.clinic.dto.*
import com.amak.clinic.service.DoctorService
import com.amak.clinic.service.PatientService
import com.amak.clinic.service.VisitService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Validated
@RestController
@RequestMapping(value = ["/api"])
class ClinicController {

    private lateinit var patientService: PatientService
    private lateinit var doctorService: DoctorService
    private lateinit var visitService: VisitService

    @Autowired
    fun ClinicController(patientService: PatientService, doctorService: DoctorService, visitService: VisitService) {
        this.patientService = patientService
        this.doctorService = doctorService
        this.visitService = visitService
    }

    @GetMapping(path = ["/patients"], produces = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun getPatients(): List<PatientResponseDTO> {
        return patientService.getPatients()
    }

    @GetMapping(path = ["/patients/{id}"], produces = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun getPatient(@PathVariable id: Long): PatientResponseDTO {
        return patientService.getPatient(id)
    }

    @PostMapping(path = ["/patients"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.CREATED)
    fun addPatient(@RequestBody @Valid patientDTO: PatientDTO) {
        patientService.addPatient(patientDTO)
    }

    @PutMapping(path = ["/patients/{id}"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun updatePatient(@PathVariable id: Long, @RequestBody @Valid patientDTO: PatientDTO) {
        patientService.updatePatient(id, patientDTO)
    }

    @DeleteMapping(value = ["/patients/{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deletePatient(@PathVariable id: Long) {
        patientService.deletePatient(id)
    }

    @GetMapping(path = ["/doctors"], produces = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun getDoctors(): List<DoctorResponseDTO> {
        return doctorService.getDoctors()
    }

    @GetMapping(path = ["/doctors/{id}"], produces = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun getDoctor(@PathVariable id: Long): DoctorResponseDTO {
        return doctorService.getDoctor(id)
    }

    @PostMapping(path = ["/doctors"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.CREATED)
    fun addDoctor(@RequestBody @Valid doctorDTO: DoctorDTO) {
        doctorService.addDoctor(doctorDTO)
    }

    @PutMapping(path = ["/doctors/{id}"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun updateDoctor(@PathVariable id: Long, @RequestBody @Valid doctorDTO: DoctorDTO) {
        doctorService.updateDoctor(id, doctorDTO)
    }

    @DeleteMapping(value = ["/doctors/{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteDoctor(@PathVariable id: Long) {
        doctorService.deleteDoctor(id)
    }

    @GetMapping(path = ["/patients/visits"], produces = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun getVisits(): List<VisitResponseDTO> {
        return visitService.getVisits()
    }

    @GetMapping(path = ["/patients/{id}/visits"], produces = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun getVisitsByPatient(@PathVariable id: Long): List<VisitResponseDTO> {
        return visitService.getVisitsByPatient(id)
    }

    @PostMapping(path = ["/patients/{id}/visits"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.CREATED)
    fun addVisit(@PathVariable id: Long, @RequestBody @Valid visitDTO: VisitDTO) {
        visitService.addVisit(id, visitDTO)
    }

    @PutMapping(path = ["/patients/visits/{id}"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun updateVisit(@PathVariable id: Long, @RequestBody @Valid visitHourDTO: VisitHourDTO) {
        visitService.updateVisit(id, visitHourDTO)
    }

    @DeleteMapping(value = ["/patients/visits/{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteVisit(@PathVariable id: Long) {
        visitService.deleteVisit(id)
    }

}