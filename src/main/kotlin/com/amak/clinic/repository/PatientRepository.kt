package com.amak.clinic.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PatientRepository : CrudRepository<Patient, Long> {
}