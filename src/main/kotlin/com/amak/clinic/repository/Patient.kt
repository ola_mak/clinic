package com.amak.clinic.repository

import javax.persistence.*

@Entity
data class Patient(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long,
        var name: String,
        var surname: String,
        var address: String,
        @OneToMany(
                mappedBy = "patient",
                cascade = [CascadeType.ALL]
        )
        var visit: List<Visit>

)