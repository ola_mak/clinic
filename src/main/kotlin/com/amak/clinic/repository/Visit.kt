package com.amak.clinic.repository

import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.*

@Entity
data class Visit(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long,
        var date: LocalDate,
        var hour: LocalTime,
        var place: String,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "doctor_id")
        var doctor: Doctor,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "patient_id")
        var patient: Patient
)