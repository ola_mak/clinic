package com.amak.clinic.repository

import javax.persistence.*

@Entity
data class Doctor(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long,
        var name: String,
        var surname: String,
        var specialization: String,
        @OneToMany(
                mappedBy = "doctor",
                cascade = [CascadeType.ALL]
        )
        var visit: List<Visit>
)