package com.amak.clinic.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.LocalDate
import java.time.LocalTime

@Repository
interface VisitRepository : CrudRepository<Visit, Long> {

    @Query("SELECT v FROM Visit v WHERE v.patient.id = :patientId")
    fun getVisitsByPatient(patientId: Long): List<Visit>

    @Query("SELECT v FROM Visit v WHERE v.doctor.id = :doctorId AND v.date = :date AND v.hour = :hour")
    fun findByDoctorInGivenTime(doctorId: Long?, date: LocalDate, hour: LocalTime): Visit?

    @Query("SELECT v FROM Visit v WHERE v.patient.id = :patientId AND v.date = :date AND v.hour = :hour")
    fun findByPatientInGivenTime(patientId: Long?, date: LocalDate, hour: LocalTime): Visit?
}
