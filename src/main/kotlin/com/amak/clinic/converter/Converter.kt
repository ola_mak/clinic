package com.amak.clinic.converter

import com.amak.clinic.dto.*
import com.amak.clinic.repository.Doctor
import com.amak.clinic.repository.Patient
import com.amak.clinic.repository.Visit
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.LocalTime

@Component
class Converter {

    private var modelMapper: ModelMapper

    @Autowired
    constructor(modelMapper: ModelMapper) {
        this.modelMapper = modelMapper
    }

    fun convertPatientEntityToResponseDTO(patient: Patient): PatientResponseDTO {
        return modelMapper.map(patient, PatientResponseDTO::class.java)
    }

    fun convertPatientDTOToEntity(patientDTO: PatientDTO): Patient {
        return modelMapper.map(patientDTO, Patient::class.java)
    }

    fun convertDoctorEntityToResponseDTO(doctor: Doctor): DoctorResponseDTO {
        return modelMapper.map(doctor, DoctorResponseDTO::class.java)
    }

    fun convertDoctorDTOToEntity(doctorDTO: DoctorDTO): Doctor {
        return modelMapper.map(doctorDTO, Doctor::class.java)
    }

    fun convertVisitEntityToResponseDTO(visit: Visit): VisitResponseDTO {
        return modelMapper.map(visit, VisitResponseDTO::class.java)
    }

    fun convertVisitDTOToEntity(visitDTO: VisitDTO): Visit {
        val visit: Visit = modelMapper.map(visitDTO, Visit::class.java)
        visit.date = LocalDate.parse(visitDTO.date)
        visit.hour = LocalTime.parse(visitDTO.hour)
        return visit
    }
}