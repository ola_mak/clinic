package com.amak.clinic

import com.amak.clinic.dto.DoctorDTO
import com.amak.clinic.dto.PatientDTO
import com.amak.clinic.dto.VisitDTO
import com.amak.clinic.dto.VisitHourDTO
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
internal class ClinicControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Test
    fun testVisitApiHappyPath() {
        //given
        val postPatientRequest: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(createPatientDTO(), HttpHeaders())
        val postDoctorRequest: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(createDoctorDTO(), HttpHeaders())
        val postVisitDTO: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(createVisitDTO(), HttpHeaders())

        //when
        testRestTemplate.postForEntity("/api/patients", postPatientRequest, String::class.java)
        testRestTemplate.postForEntity("/api/doctors", postDoctorRequest, String::class.java)
        val postVisitResult = testRestTemplate.postForEntity("/api/patients/1/visits", postVisitDTO, String::class.java)
        val getVisitsResult = testRestTemplate.getForEntity("/api/patients/visits", String::class.java)
        val getVisitByPatientResult = testRestTemplate.getForEntity("/api/patients/1/visits", String::class.java)

        //then
        assertEquals(postVisitResult.statusCode, HttpStatus.CREATED)
        assertEquals(getVisitsResult.statusCode, HttpStatus.OK)
        assertEquals(getVisitsResult.body, "[{\"id\":3,\"date\":\"2021-06-30\",\"hour\":\"16:00\",\"place\":\"Poznań\",\"doctorId\":2,\"patientId\":1}]")
        assertEquals(getVisitByPatientResult.statusCode, HttpStatus.OK)
        assertEquals(getVisitByPatientResult.body, "[{\"id\":3,\"date\":\"2021-06-30\",\"hour\":\"16:00\",\"place\":\"Poznań\",\"doctorId\":2,\"patientId\":1}]")

        //given
        val putRequest: HttpEntity<VisitHourDTO> = HttpEntity<VisitHourDTO>(VisitHourDTO("16:30:00"), HttpHeaders())

        //when
        val putResult = testRestTemplate.exchange("/api/patients/visits/3", HttpMethod.PUT, putRequest, String::class.java)
        val getVisitAfterUpdateResult = testRestTemplate.getForEntity("/api/patients/visits", String::class.java)

        //then
        assertEquals(putResult.statusCode, HttpStatus.OK)
        assertEquals(getVisitAfterUpdateResult.body, "[{\"id\":3,\"date\":\"2021-06-30\",\"hour\":\"16:30\",\"place\":\"Poznań\",\"doctorId\":2,\"patientId\":1}]")

        //when
        val deleteResult = testRestTemplate.exchange("/api/patients/visits/3", HttpMethod.DELETE, HttpEntity<String>("", HttpHeaders()), String::class.java)
        val getResult3 = testRestTemplate.getForEntity("/api/patients/visits", String::class.java)

        //then
        assertEquals(deleteResult.statusCode, HttpStatus.NO_CONTENT)
        assertEquals(getResult3.body, "[]")
    }

    @Test
    fun testVisitApiExceptionsHandling() {
        //given
        val postPatientRequest: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(createPatientDTO(), HttpHeaders())
        val postDoctorRequest: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(createDoctorDTO(), HttpHeaders())
        val postRequest1: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(VisitDTO("", "16:00:00", "Poznań", 2), HttpHeaders())
        val postRequest2: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(createVisitDTO(), HttpHeaders())
        val postRequest3: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(VisitDTO("1410-07-15", "09:00:00", "Pod Grunwaldem", 2), HttpHeaders())
        val postRequest4: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(VisitDTO("2022-06-12", "09:15:00", "Poznań", 2), HttpHeaders())
        val postRequest5: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(VisitDTO("2022-06-12", "09:00:00", "Poznań", 2222), HttpHeaders())
        val postRequest6: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(createVisitDTO(), HttpHeaders())
        val postRequest7: HttpEntity<VisitDTO> = HttpEntity<VisitDTO>(VisitDTO("2021-06-30", "16:00:00", "Poznań", 3), HttpHeaders())
        val putRequest1: HttpEntity<VisitHourDTO> = HttpEntity<VisitHourDTO>(VisitHourDTO("9"), HttpHeaders())
        val putRequest2: HttpEntity<VisitHourDTO> = HttpEntity<VisitHourDTO>(VisitHourDTO("18:00:00"), HttpHeaders())

        //when
        testRestTemplate.postForEntity("/api/patients", postPatientRequest, String::class.java)
        testRestTemplate.postForEntity("/api/doctors", postDoctorRequest, String::class.java)
        testRestTemplate.postForEntity("/api/doctors", postDoctorRequest, String::class.java)
        val postResult1 = testRestTemplate.postForEntity("/api/patients/1/visits", postRequest1, String::class.java)
        val postResult2 = testRestTemplate.postForEntity("/api/patients/111/visits", postRequest2, String::class.java)
        val postResult3 = testRestTemplate.postForEntity("/api/patients/1/visits", postRequest3, String::class.java)
        val postResult4 = testRestTemplate.postForEntity("/api/patients/1/visits", postRequest4, String::class.java)
        val postResult5 = testRestTemplate.postForEntity("/api/patients/1/visits", postRequest5, String::class.java)
        testRestTemplate.postForEntity("/api/patients/1/visits", postRequest6, String::class.java)
        val postResult6 = testRestTemplate.postForEntity("/api/patients/1/visits", postRequest6, String::class.java)
        val postResult7 = testRestTemplate.postForEntity("/api/patients/1/visits", postRequest7, String::class.java)
        val getResult1 = testRestTemplate.getForEntity("/api/patients//111/visits", String::class.java)
        val putResult1 = testRestTemplate.exchange("/api/patients/visits/3", HttpMethod.PUT, putRequest1, String::class.java)
        val putResult2 = testRestTemplate.exchange("/api/patients/visits/333", HttpMethod.PUT, putRequest2, String::class.java)
        val deleteResult = testRestTemplate.exchange("/api/patients/visits/111", HttpMethod.DELETE, HttpEntity<String>("", HttpHeaders()), String::class.java)

        //then
        assertEquals(postResult1.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(postResult1.body, "Please use dd-MMM-yyyy format of date. The appointment can only be scheduled for the following days.")
        assertEquals(postResult2.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(postResult2.body, "There is no patient with given id")
        assertEquals(postResult3.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(postResult3.body, "Please use dd-MMM-yyyy format of date. The appointment can only be scheduled for the following days.")
        assertEquals(postResult4.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(postResult4.body, "Please use hh:mm:ss format of time, appointment can be scheduled every half hour ")
        assertEquals(postResult5.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(postResult5.body, "There is no doctor with given id")
        assertEquals(postResult6.statusCode, HttpStatus.CONFLICT)
        assertEquals(postResult6.body, "The appointment date is already taken")
        assertEquals(postResult7.statusCode, HttpStatus.CONFLICT)
        assertEquals(postResult7.body, "Patient already has an appointment within given period")

        assertEquals(getResult1.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(getResult1.body, "There is no patient with given id")

        assertEquals(putResult1.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(putResult1.body, "Please use hh:mm:ss format of time, appointment can be scheduled every half hour ")
        assertEquals(putResult2.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(putResult2.body, "There is no visit with given id")

        assertEquals(deleteResult.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(deleteResult.body, "There is no visit with given id")

    }

    @Test
    fun testPatientApiHappyPath() {
        //given
        val postRequest: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(createPatientDTO(), HttpHeaders())

        //when
        val postResult = testRestTemplate.postForEntity("/api/patients", postRequest, String::class.java)
        val getResult1 = testRestTemplate.getForEntity("/api/patients", String::class.java)

        //then
        assertEquals(postResult.statusCode, HttpStatus.CREATED)
        assertEquals(getResult1.statusCode, HttpStatus.OK)
        assertEquals(getResult1.body, "[{\"id\":1,\"name\":\"Tomasz\",\"surname\":\"Nowak\",\"address\":\"Wichrowe Wzgórze 1, Poznań\"}]")

        //given
        val putRequest: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(updatePatientDTO(), HttpHeaders())

        //when
        val putResult = testRestTemplate.exchange("/api/patients/1", HttpMethod.PUT, putRequest, String::class.java)
        val getResult2 = testRestTemplate.getForEntity("/api/patients", String::class.java)

        //then
        assertEquals(putResult.statusCode, HttpStatus.OK)
        assertEquals(getResult2.body, "[{\"id\":1,\"name\":\"Tomasz\",\"surname\":\"Nowak\",\"address\":\"Polanka 124, Poznań\"}]")

        //when
        val deleteResult = testRestTemplate.exchange("/api/patients/1", HttpMethod.DELETE, HttpEntity<String>("", HttpHeaders()), String::class.java)
        val getResult3 = testRestTemplate.getForEntity("/api/patients", String::class.java)

        //then
        assertEquals(deleteResult.statusCode, HttpStatus.NO_CONTENT)
        assertEquals(getResult3.body, "[]")
    }

    @Test
    fun testPatientApiExceptionsHandling() {
        //given
        val postRequest: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(PatientDTO("", "Nowak", "Poznań"), HttpHeaders())
        val putRequest1: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(PatientDTO("", "Nowak", "Poznań"), HttpHeaders())
        val putRequest2: HttpEntity<PatientDTO> = HttpEntity<PatientDTO>(createPatientDTO(), HttpHeaders())

        //when
        val postResult = testRestTemplate.postForEntity("/api/patients", postRequest, String::class.java)
        val getResult = testRestTemplate.getForEntity("/api/patients/111", String::class.java)
        val putResult1 = testRestTemplate.exchange("/api/patients/1", HttpMethod.PUT, putRequest1, String::class.java)
        val putResult2 = testRestTemplate.exchange("/api/patients/111", HttpMethod.PUT, putRequest2, String::class.java)
        val deleteResult = testRestTemplate.exchange("/api/patients/111", HttpMethod.DELETE, HttpEntity<String>("", HttpHeaders()), String::class.java)

        //then
        assertEquals(postResult.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(postResult.body, "Name cannot be blank")

        assertEquals(getResult.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(getResult.body, "There is no patient with given id")

        assertEquals(putResult1.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(putResult1.body, "Name cannot be blank")

        assertEquals(putResult2.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(putResult2.body, "There is no patient with given id")

        assertEquals(deleteResult.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(deleteResult.body, "There is no patient with given id")
    }

    @Test
    fun testDoctorApiHappyPath() {
        //given
        val postRequest: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(createDoctorDTO(), HttpHeaders())

        //when
        val postResult = testRestTemplate.postForEntity("/api/doctors", postRequest, String::class.java)
        val getResult1 = testRestTemplate.getForEntity("/api/doctors", String::class.java)

        //then
        assertEquals(postResult.statusCode, HttpStatus.CREATED)
        assertEquals(getResult1.statusCode, HttpStatus.OK)
        assertEquals(getResult1.body, "[{\"id\":1,\"name\":\"Robert\",\"surname\":\"Kowalski\",\"specialization\":\"Chirurg\"}]")

        //given
        val putRequest: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(updateDoctorDTO(), HttpHeaders())

        //when
        val putResult = testRestTemplate.exchange("/api/doctors/1", HttpMethod.PUT, putRequest, String::class.java)
        val getResult2 = testRestTemplate.getForEntity("/api/doctors", String::class.java)

        //then
        assertEquals(putResult.statusCode, HttpStatus.OK)
        assertEquals(getResult2.body, "[{\"id\":1,\"name\":\"Tomasz\",\"surname\":\"Kowalski\",\"specialization\":\"Chirurg\"}]")

        //when
        val deleteResult = testRestTemplate.exchange("/api/doctors/1", HttpMethod.DELETE, HttpEntity<String>("", HttpHeaders()), String::class.java)
        val getResult3 = testRestTemplate.getForEntity("/api/doctors", String::class.java)

        //then
        assertEquals(deleteResult.statusCode, HttpStatus.NO_CONTENT)
        assertEquals(getResult3.body, "[]")
    }

    @Test
    fun testDoctortApiExceptionsHandling() {
        //given
        val postRequest: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(DoctorDTO("", "Nowak", "Chirurg"), HttpHeaders())
        val putRequest1: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(DoctorDTO("", "Nowak", "Chirurg"), HttpHeaders())
        val putRequest2: HttpEntity<DoctorDTO> = HttpEntity<DoctorDTO>(createDoctorDTO(), HttpHeaders())

        //when
        val postResult = testRestTemplate.postForEntity("/api/doctors", postRequest, String::class.java)
        val getResult = testRestTemplate.getForEntity("/api/doctors/111", String::class.java)
        val putResult1 = testRestTemplate.exchange("/api/doctors/1", HttpMethod.PUT, putRequest1, String::class.java)
        val putResult2 = testRestTemplate.exchange("/api/doctors/111", HttpMethod.PUT, putRequest2, String::class.java)
        val deleteResult = testRestTemplate.exchange("/api/doctors/111", HttpMethod.DELETE, HttpEntity<String>("", HttpHeaders()), String::class.java)

        //then
        assertEquals(postResult.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(postResult.body, "Name cannot be blank")

        assertEquals(getResult.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(getResult.body, "There is no doctor with given id")

        assertEquals(putResult1.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(putResult1.body, "Name cannot be blank")

        assertEquals(putResult2.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(putResult2.body, "There is no doctor with given id")

        assertEquals(deleteResult.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(deleteResult.body, "There is no doctor with given id")
    }

    private fun createPatientDTO(): PatientDTO {
        return PatientDTO("Tomasz", "Nowak", "Wichrowe Wzgórze 1, Poznań")
    }

    private fun updatePatientDTO(): PatientDTO {
        return PatientDTO("Tomasz", "Nowak", "Polanka 124, Poznań")
    }

    private fun createVisitDTO(): VisitDTO {
        return VisitDTO("2021-06-30", "16:00:00", "Poznań", 2)
    }

    private fun createDoctorDTO(): DoctorDTO {
        return DoctorDTO("Robert", "Kowalski", "Chirurg")
    }

    private fun updateDoctorDTO(): DoctorDTO {
        return DoctorDTO("Tomasz", "Kowalski", "Chirurg")
    }


}