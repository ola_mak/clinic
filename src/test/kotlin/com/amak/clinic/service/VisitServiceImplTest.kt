package com.amak.clinic.service

import AppointmentIsTakenException
import PatientHasAppointmentException
import com.amak.clinic.converter.Converter
import com.amak.clinic.dto.VisitDTO
import com.amak.clinic.repository.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import kotlin.collections.ArrayList


@RunWith(SpringRunner::class)
@SpringBootTest
internal class VisitServiceImplTest {

    @InjectMocks
    lateinit var visitServiceImpl: VisitServiceImpl

    @Mock
    lateinit var doctorRepository: DoctorRepository

    @Mock
    lateinit var patientRepository: PatientRepository

    @Mock
    lateinit var visitRepository: VisitRepository

    @Mock
    lateinit var converter: Converter

    @Before
    fun init() {
        val doctor: Optional<Doctor> = Optional.ofNullable(Doctor(1, "", "", "", ArrayList<Visit>()))
        val patient: Optional<Patient> = Optional.ofNullable(Patient(1, "", "", "", ArrayList<Visit>()))
        val visit: Visit? = Visit(1, LocalDate.parse("2021-06-30"), LocalTime.parse("16:00:00"), "", Doctor(1, "", "", "", ArrayList<Visit>()), Patient(1, "", "", "", ArrayList<Visit>()))
        val visitDTO = createVisitDTO()

        Mockito.`when`(doctorRepository.findById(1)).thenReturn(doctor)
        Mockito.`when`(converter.convertVisitDTOToEntity(visitDTO)).thenReturn(visit)
        Mockito.`when`(patientRepository.findById(1)).thenReturn(patient)
    }

    @Test
    fun shouldSaveVisitIfDoctorAndPatientHaveFreeDate() {
        // given
        val visit: Visit? = createVisit()
        val visitDTO = createVisitDTO()

        // when
        Mockito.`when`(visitRepository.findByDoctorInGivenTime(visitDTO.doctorId, LocalDate.parse(visitDTO.date), LocalTime.parse(visitDTO.hour))).thenReturn(null)
        Mockito.`when`(visitRepository.findByPatientInGivenTime(visitDTO.doctorId, LocalDate.parse(visitDTO.date), LocalTime.parse(visitDTO.hour))).thenReturn(null)
        visitServiceImpl.addVisit(1, visitDTO)

        //then
        if (visit != null) {
            verify(visitRepository).save(visit)
        }
    }

    @Test(expected = AppointmentIsTakenException::class)
    fun shouldThrowExceptionWhenAppointmentIsTaken() {
        // given
        val visit: Visit? = createVisit()
        val visitDTO: VisitDTO = createVisitDTO()

        // when
        Mockito.`when`(visitRepository.findByDoctorInGivenTime(1, LocalDate.parse("2021-06-30"), LocalTime.parse("16:00:00"))).thenReturn(visit)
        visitServiceImpl.addVisit(1, visitDTO)
    }

    @Test(expected = PatientHasAppointmentException::class)
    fun shouldThrowExceptionIfPatientHasAppointmentWithinGivenPeriod() {
        // given
        val visit: Visit? = createVisit()
        val visitDTO: VisitDTO = createVisitDTO()

        //when
        Mockito.`when`(visitRepository.findByDoctorInGivenTime(1, LocalDate.parse("2021-06-30"), LocalTime.parse("16:00:00"))).thenReturn(null)
        Mockito.`when`(visitRepository.findByPatientInGivenTime(1, LocalDate.parse("2021-06-30"), LocalTime.parse("16:00:00"))).thenReturn(visit)
        visitServiceImpl.addVisit(1, visitDTO)
    }

    private fun createVisitDTO(): VisitDTO {
        return VisitDTO("2021-06-30", "16:00:00", "", 1)
    }

    private fun createVisit(): Visit? {
        return Visit(1, LocalDate.parse("2021-06-30"), LocalTime.parse("16:00:00"), "",
                Doctor(1, "", "", "", ArrayList<Visit>()), Patient(1, "", "", "", ArrayList<Visit>()))
    }


}