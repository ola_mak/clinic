# Clinic

##Backend

Backend is spring-boot application written in Kotlin

##Build application

To build and install artifacts into the local repository:

./gradlew clean build

##Run tests

./gradlew test

